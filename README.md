## Tareas Redes 2019-2

En este repositorio estaremos manejando las tareas de la materia de [redes de computadoras][url-redes-gitlab] que se imparte en la [Facultad de Ciencias, UNAM][url-redes-fciencias] en el semestre 2019-2.

[url-redes-gitlab]: https://Redes-Ciencias-UNAM.gitlab.io/ "Página en GitLab"
[url-redes-fciencias]: http://www.fciencias.unam.mx/docencia/horarios/20192/1556/714 "Redes 2019-2"
