Para compilar el archivo se debera ejecutar la siguiente linea:
gcc -o hashes hashes.c -lcrypto -lssl
Ejemplos de ejecucion:
./hashes md5 Hola.txt 
./hashes sha256 Hola.txt 
./hashes sha1 Hola.txt

Si manda un error del archivo md5.h instalar con la siguiente linea:
apt-get install libssl-dev
